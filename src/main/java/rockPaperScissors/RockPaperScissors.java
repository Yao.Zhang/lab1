package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
   
    public void run() {
        // TODO: Implement Rock Paper Scissors
        String userChoice;
        String computerChoice;
        String playAgain;
    
        while(true) {
            System.out.println("Let's play round " + roundCounter);            
            
            while(true) {
                userChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                // En uendelig loop som vil gå frem, helt til brukeren velger rock, paper or scissor.
                if(userChoice.equals("rock") || userChoice.equals("paper") || userChoice.equals("scissors")) {
                    break;
                }
                System.out.print("I do not understand " + userChoice + ". ");
                System.out.println("Could you try again?");
            }

            computerChoice = rpsChoices.get(new Random().nextInt(rpsChoices.size())).toLowerCase();
            System.out.print("Human chose " + userChoice + ", computer chose " + computerChoice);
            // Hvis begge velger det samme. 
    
            if (userChoice.equals(computerChoice)) {
                System.out.println(". It's a tie!");
            }
                
            // Human is choosing rock
            else if(userChoice.equals("rock")) {
            // Human is choosing paper
                if(computerChoice.equals("scissors")){
                    humanScore++;
                    System.out.println(". Human wins!");
                }
            //Human is choosing scissors
                else {
                    computerScore++;
                    System.out.println(". Computer wins!");
                }
            }
            else if(userChoice.equals("paper")) {
                if(computerChoice.equals("rock")){
                    humanScore++;
                    System.out.println(". Human wins!");
                }
                else {
                    computerScore++;
                    System.out.println(". Computer wins!");
                }
            }
            // Human is choosing scissors
            else {
                if(computerChoice.equals("paper")){
                    humanScore++;
                    System.out.println(". Human wins!");
                }
                else {
                    computerScore++;
                    System.out.println(". Computer wins!");
                }
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    
            while(true) {
            playAgain = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();

                if (playAgain.equals("n") || playAgain.equals("y")) {
                    roundCounter++;
                    break;
                }
                System.out.print("I do not understand " + playAgain + ". ");
                System.out.println("Could you try again?");
        
            }
            if (playAgain.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        }
    }
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
